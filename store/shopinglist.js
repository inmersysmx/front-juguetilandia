export const state = () => ({
    listItems: [],
    isShopingCarListVisible: false,
    isShopingCarListOk: false,
    letterId: "",
    userId: "",
    nameToSanta: "",
    isSantaVideoModalShown: false,
    isAddedCartModalShown: false
});

export const mutations = {
    toggleShopingCarListVisible(state) {
        state.isShopingCarListVisible = !state.isShopingCarListVisible;
    },
    async deleteItem(state, key) {
        try {
            const newListItems = [];
            state.listItems = state.listItems.filter(item => {
                if (item._id !== key) {
                    newListItems.push(item._id);
                    return item;
                }
            });
            console.log( "Carrito que mando: ", newListItems );
            let user =  localStorage.getItem('_id')
            const letterData = {
                user,
                status: "LOADING",
                products: newListItems
            };
            const { data } = await this.$api.card.update(state.letterId, letterData);
            if (data.status.code === "0000") {
                console.log("DELETE RESPONSE: ", data);
                console.log(data.result.card);
            }
        } catch (error) {
            console.log("Ocurrio un error", error);
        }
    },
    async sendLetter(state) {
        try {
            const newListItems = [];

            state.listItems.forEach(item => {
                newListItems.push(item._id);
            });
            console.log( newListItems );
            // let user = this.$auth.$storage.getLocalStorage('userId')
            let user = localStorage.getItem("_id");
            const letterData = {
                user,
                status: "SENDED",
                products: newListItems
            };
            console.log(letterData);
            const { data } = await this.$api.card.update(state.letterId, letterData);
            if (data.status.code === "0000") {
                console.log("UPDATE RESPONSE: ", data);
                console.log(data.result.card);
            }
            
        } catch (error) {
            console.log("Ocurrio un error", error);
        }
    },

    setListItems(state, data) {
        state.listItems = data;
    },
    setLetterId(state, id) {
        state.letterId = id;
    },
    setUserId(state, id) {
        state.userId = id;
    },
    setIsShopingCarListOk(state, status) {
        state.isShopingCarListOk = status;
    },
    setNameToSanta(state, name) {
        state.nameToSanta = name;
    },
    setIsSantaVideoModalShown(state, status) {
        state.isSantaVideoModalShown = status;
    },
    setIsAddedCartModalShown(state, status) {
        state.isAddedCartModalShown = status;
    }
};