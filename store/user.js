export const state = () => ({
    userId: '',
    userEmail: '',
})
export const mutations = {
    setUserId(state, id) {
        state.userId = id
    },
    setUserEmail(state, email) {
        state.userEmail = email
    }
}