const resource = 'cards'

export default http => ({
    create(userData) {
        return http.post(`${resource}/createCard`, userData)
    },
    update(cardId, cardData) {
        return http.patch(`${resource}/${cardId}`, cardData)
    },
    getByUserAndStatus(userId, status) {
        return http.get(`${resource}/getAllCardsByUserId?userId=${userId}&status=${status}`)
    },
    getSantaVideo(name) {
        return http.get(`${resource}/getVideo?name=${name}`)
    }
})