const resource = 'stopMotion'

export default http => ({
  getByFilter (filter) {
    return http.get(`${resource}/pagination/1?${filter}`)
  },
  getViewCount (videoId) {
    return http.get(`${resource}/${videoId}`)
  },
  getByUserIdAndStatus (userId, status) {
    return http.get(`${resource}/pagination/1?limit=10000&userId=${userId}&sortDate=DESC&status=${status}`)
  },
  getFeed () {
    return http.get(`${resource}/pagination/1?limit=10000&sortDate=DESC&status=APPROVED`)
  },
  create (stopMotionData) {
    return http.post(`${resource}`, stopMotionData)
  }
})
