const resource = 'books'

export default http => ({
  getAll () {
    return http.get(`${resource}/pagination/:skip`)
  },
  getById (bookId) {
    return http.get(`${resource}/${bookId}`)
  },
})
