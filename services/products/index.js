const resource = 'products'

export default http => ({
  getAllTradeMarks () {
    return http.get(`${resource}/getAllTrademarks`)
  },
  getByTrademark (idTradeMark) {
    return http.get(`${resource}/pagination/1?limit=10000&trademarks=${idTradeMark}`)
  },
  getById (idProduct) {
    return http.get(`${resource}/${idProduct}`)
  }
})
