const resource = 'events'

export default http => ({
  getByDatesRange (lowerDate, greaterDate) {
    return http.get(`${resource}/pagination/1?limit=10000&status=CREATED&greaterDate=${greaterDate}&lowerDate=${lowerDate}`)
  },
  getAll () {
    return http.get(`${resource}/pagination/1?limit=10000&status=CREATED`)
  },
  getById (eventId) {
    return http.get(`${resource}/${eventId}`)
  },
  getEvent360Meny (eventDate) {
    return http.get(`${resource}/pagination/1?status=CREATED&greaterDate=${eventDate}&lowerDate=2021-12-12&limit=1000`)
  }
})
