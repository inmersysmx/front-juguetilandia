const resource = 'trademarks'

export default http => ({
  getById (id) {
    return http.get(`${resource}/${id}`)
  }
})
