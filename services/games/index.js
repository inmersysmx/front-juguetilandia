const resource = 'games'

export default http => ({
  getAllGames () {
    return http.get(`${resource}/getAllByFilter/1?limit=25`)
  }
})
