import Repository from '~/services/RepositoryFactory'

export default function ({ $axios }, inject) {
  // Create a custom axios instance
  const api = $axios.create({
    baseURL: 'https://dev.losmagicians.mx/api/'
    // baseURL: 'http://198.211.104.71/api/'
  })

  // Inject to context as $api
  inject('api', Repository(api))
}
